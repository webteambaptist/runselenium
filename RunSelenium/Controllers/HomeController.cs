﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RunSelenium.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunSelenium.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
        
        [HttpPost]
        public IActionResult RunBpp()
        {
            //var targetDir = string.Format(@"C:\Projects2020\Selenium\Selenium\SeleniumPP\bin\Debug\netcoreapp3.0\");
            var p = new Process
            {
                StartInfo = {FileName = @"C:\Projects2020\Selenium\Selenium\SeleniumPP\bin\Debug\netcoreapp3.0\SeleniumPP.exe", CreateNoWindow = false}
            };
            p.Start();
            p.WaitForExit();
            return RedirectToAction("Index");

        }

        [HttpPost]
        public IActionResult RunPp()
        {
            var p = new Process {
                StartInfo = {FileName = @"C:\Projects2020\Selenium\Selenium\SeleniumBPP\bin\Debug\netcoreapp3.0\SeleniumBPP.exe", CreateNoWindow = false}
            };
            p.Start();
            p.WaitForExit();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult RunPrimecomp()
        {
            var p = new Process {
                StartInfo = {FileName = @"C:\Projects2020\Selenium\Selenium\SeleniumPrimecomp\bin\Debug\netcoreapp3.1\SeleniumPrimecomp.exe", CreateNoWindow = false}
            };
            p.Start();
            p.WaitForExit();
            return RedirectToAction("Index");
        }
    }
}
